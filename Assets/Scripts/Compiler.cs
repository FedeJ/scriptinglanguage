﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Compiler 
{
	Tables tables = new Tables();
	Parser parser;

    //ATENCION
    //Aca estan los datos que usa el parser para saber si los datos fueron bien escritos en consola
    public Compiler()
    {
        parser = new Parser(tables);

        tables.AddInstrLookUp("MOV", OpCodes.INSTR_MOV, 2);
        tables.SetOpType("MOV", 0,
            OpFlags.MemIdx
        );
        tables.SetOpType("MOV", 1,
            OpFlags.Literal |
            OpFlags.MemIdx
        );

        tables.AddInstrLookUp("ADD", OpCodes.INSTR_ADD, 2);
        tables.SetOpType("ADD", 0,
            OpFlags.MemIdx
        );
        tables.SetOpType("ADD", 1,
            OpFlags.Literal |
            OpFlags.MemIdx
        );

        tables.AddInstrLookUp("SUB", OpCodes.INSTR_SUB, 2);
        tables.SetOpType("SUB", 0,
            OpFlags.MemIdx
        );
        tables.SetOpType("SUB", 1,
            OpFlags.Literal |
            OpFlags.MemIdx
        );

        tables.AddInstrLookUp("MUL", OpCodes.INSTR_MUL, 2);
        tables.SetOpType("MUL", 0,
            OpFlags.MemIdx
        );
        tables.SetOpType("MUL", 1,
            OpFlags.Literal |
            OpFlags.MemIdx
        );

        tables.AddInstrLookUp("DIV", OpCodes.INSTR_DIV, 2);
        tables.SetOpType("DIV", 0,
            OpFlags.MemIdx
        );
        tables.SetOpType("DIV", 1,
            OpFlags.Literal |
            OpFlags.MemIdx
        );

        tables.AddInstrLookUp("MOD", OpCodes.INSTR_MOD, 2);
        tables.SetOpType("MOD", 0,
            OpFlags.MemIdx
        );
        tables.SetOpType("MOD", 1,
            OpFlags.Literal |
            OpFlags.MemIdx
        );

        tables.AddInstrLookUp("EXP", OpCodes.INSTR_EXP, 2);
        tables.SetOpType("EXP", 0,
            OpFlags.MemIdx
        );
        tables.SetOpType("EXP", 1,
            OpFlags.Literal |
            OpFlags.MemIdx
        );

        //tables.AddInstrLookUp("NEG", OpCodes.INSTR_NEG, 0);

        tables.AddInstrLookUp("INC", OpCodes.INSTR_INC, 1);
        tables.SetOpType("INC", 0,
            OpFlags.MemIdx
        );

        tables.AddInstrLookUp("DEC", OpCodes.INSTR_DEC, 1);
        tables.SetOpType("DEC", 0,
            OpFlags.MemIdx
        );

        //tables.AddInstrLookUp("AND", OpCodes.INSTR_AND, 0);
        //tables.AddInstrLookUp("OR", OpCodes.INSTR_OR, 0);
        //tables.AddInstrLookUp("XOR", OpCodes.INSTR_XOR, 0);
        //tables.AddInstrLookUp("NOR", OpCodes.INSTR_NOT, 0);

        tables.AddInstrLookUp("SHL", OpCodes.INSTR_SHL, 1);
        tables.SetOpType("SHL", 0,
            OpFlags.MemIdx
        );

        tables.AddInstrLookUp("SHR", OpCodes.INSTR_SHR, 1);
        tables.SetOpType("SHR", 0,
            OpFlags.MemIdx
        );

        tables.AddInstrLookUp("JMP", OpCodes.INSTR_JMP, 1);
        tables.SetOpType("JMP", 0,
            OpFlags.InstrIdx
        );

        tables.AddInstrLookUp("JE", OpCodes.INSTR_JE, 3);
        tables.SetOpType("JE", 0,
            OpFlags.InstrIdx
        );
        tables.SetOpType("JE", 1,
            OpFlags.Literal |
            OpFlags.MemIdx
        );
        tables.SetOpType("JE", 2,
            OpFlags.Literal |
            OpFlags.MemIdx
        );

        tables.AddInstrLookUp("JNE", OpCodes.INSTR_JNE, 3);
        tables.SetOpType("JNE", 0,
            OpFlags.InstrIdx
        );
        tables.SetOpType("JNE", 1,
            OpFlags.Literal |
            OpFlags.MemIdx
        );
        tables.SetOpType("JNE", 2,
            OpFlags.Literal |
            OpFlags.MemIdx
        );

        tables.AddInstrLookUp("JG", OpCodes.INSTR_JG, 3);
        tables.SetOpType("JG", 0,
            OpFlags.InstrIdx
        );
        tables.SetOpType("JG", 1,
            OpFlags.Literal |
            OpFlags.MemIdx
        );
        tables.SetOpType("JG", 2,
            OpFlags.Literal |
            OpFlags.MemIdx
        );

        tables.AddInstrLookUp("JL", OpCodes.INSTR_JL, 3);
        tables.SetOpType("JL", 0,
            OpFlags.InstrIdx
        );
        tables.SetOpType("JL", 1,
            OpFlags.Literal |
            OpFlags.MemIdx
        );
        tables.SetOpType("JL", 2,
            OpFlags.Literal |
            OpFlags.MemIdx
        );

        tables.AddInstrLookUp("JGE", OpCodes.INSTR_JGE, 3);
        tables.SetOpType("JGE", 0,
            OpFlags.InstrIdx
        );
        tables.SetOpType("JGE", 1,
            OpFlags.Literal |
            OpFlags.MemIdx
        );
        tables.SetOpType("JGE", 2,
            OpFlags.Literal |
            OpFlags.MemIdx
        );

        tables.AddInstrLookUp("JLE", OpCodes.INSTR_JLE, 3);
        tables.SetOpType("JLE", 0,
            OpFlags.InstrIdx
        );
        tables.SetOpType("JLE", 1,
            OpFlags.Literal |
            OpFlags.MemIdx
        );
        tables.SetOpType("JLE", 2,
            OpFlags.Literal |
            OpFlags.MemIdx
        );

        tables.AddInstrLookUp("PUSH", OpCodes.INSTR_PUSH, 1);
        tables.SetOpType("PUSH", 0,
            OpFlags.MemIdx |
            OpFlags.Literal
        );


        tables.AddInstrLookUp("POP", OpCodes.INSTR_POP, 1);
        tables.SetOpType("POP", 0,
            OpFlags.MemIdx
        );

        tables.AddInstrLookUp("PAUSE", OpCodes.INSTR_PAUSE, 1);
        tables.SetOpType("PAUSE", 0,
            OpFlags.Literal |
            OpFlags.MemIdx
        );

        tables.AddInstrLookUp("EXIT", OpCodes.INSTR_EXIT, 0);

        tables.AddInstrLookUp("JFR", OpCodes.INSTR_JFR, 1);
        tables.SetOpType("JFR", 0,
            OpFlags.FuncIdx
		);

        tables.AddInstrLookUp("RET", OpCodes.INSTR_RET, 0);

        tables.AddInstrLookUp("CLH", OpCodes.INSTR_CALLHOST, 2);
		tables.SetOpType("CLH", 0, 
			OpFlags.HostAPICallStr
		);
		tables.SetOpType("CLH", 1, 
			OpFlags.Literal
		);

        tables.AddInstrLookUp("LN", OpCodes.INSTR_LN, 1);
        tables.SetOpType("LN", 0,
            OpFlags.MemIdx
        );

        tables.AddInstrLookUp("LOG", OpCodes.INSTR_LOG, 2);
        tables.SetOpType("LOG", 0,
            OpFlags.MemIdx
        );
        tables.SetOpType("LOG", 1,
            OpFlags.Literal |
            OpFlags.MemIdx
        );

        tables.AddInstrLookUp("PRINT", OpCodes.INSTR_PRINT, 1);
        tables.SetOpType("PRINT", 0,
            OpFlags.Literal |
            OpFlags.HostAPICallStr |
            OpFlags.MemIdx
        );
    }

	public Tables GetTables()
	{
		return tables;
	}

	public bool Compile(string str)
	{
		parser.Reset();
		
		if (!parser.Parse(str))
		{
			Debug.Log("Error while parsing...AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
			return false;		
		}

		return true;
	}

}
