﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices; // For aligning layouts

// ==================================================
// File format structs
public static class HeaderConst
{
	public const string Magic = "ZS"; // For Zak Script
	public const byte MajorVersion = 1; // Major version of my script
	public const byte MinorVersion = 0; // Minor version
}

public struct Header
{
	public string Magic;
	public byte MajorVersion;
	public byte MinorVersion;
	public int StackSize;
	public int GlobalVarsSize;
    public int FuncsSize;
    public int PCStartIdx;
	public int InstructionsCount;
}

// ==================================================
// Compiler structs
public struct LabelDecl
{
	public int Idx;
	public string Ident;
    public int ScopeIdx;
}

public struct InstrDecl
{
	public int OpCode;
	public int ParamsCount;
	public int[] ParamsFlags;
}

public struct VarDecl
{
	public string Ident;
	public int Idx;
    public int ScopeIdx;
    public bool isArg;
	// TODO: add func id
}

public struct FuncDecl {
    public string Ident;
    public int Idx;
    public int ScopeIdx;
    public int stackSize;
    public int argSize;
    public List<VarDecl> localVars;
    public List<VarDecl> arguments;
}

// ==================================================
// Runtime structs
public struct InstrStream
{
	public Instruction[] Instructions;
	public int PC;
	public int StartPC;
}

public struct Instruction
{
	public int OpCode;
	public Value[] Values;

	// TODO: add function id
}


public static class OpFlags
{
	//ATENCION
	public const int Literal 		= 1; // Enteros, floats y strings
	public const int MemIdx 		= 2; // Son las variables. Se llama Mem Index porque es un indice que apunta al array del stack
	public const int InstrIdx 		= 4; // Estos son las etiquetas (o labels) que luego se convierten en un indice a una instruccion
	public const int HostAPICallStr	= 8; // Identificador a la funcion del host
    public const int FuncIdx        = 16;
}

public enum OpType
{
	Null,
	Int,
	Float, 
	String, 
	GlobalMemIdx,
    RelativeMemInx,
    ArgumentMemInx,
	InstrIdx, 
	HostAPICallStr,
	HostAPICallIdx,
    FuncIdx
}


// Pack = 1 > Pack aligned on 1 byte boundaries 
// (no gaps between fields)
// Explicit means we can specify the offsets manually
[StructLayout(LayoutKind.Explicit, Pack = 1)] 
public struct Value
{
	[FieldOffset(0)]
	public OpType 	Type;
	[FieldOffset(4)]
	public int 		IntLiteral;
	[FieldOffset(4)]
	public float 	FloatLiteral;
	[FieldOffset(4)]
	public int 		StackIndex;
	[FieldOffset(4)]
	public int		InstrIndex;
	[FieldOffset(4)]
	public int		HostAPICallIndex;
	[FieldOffset(8)]
	public string 	StringLiteral;
}

public struct RuntimeStack 
{
	public Value[] Elements;
	public int StackStartIdx; // stack starts after global vars
	public int TopStackIdx;
}

public delegate void HostFuncDlg(Value[] values);

public struct HostFunc
{
	public string Ident;
	public HostFuncDlg Func;
}

public struct Function {
    public int startIndex;
    public int stackFrameSize;
    public int argSize;
}

public struct CallStack
{
    public int retIndex;
    public int startTopIdx;
}

public class ScriptContext //Maneja el contexto del script
{
	public RuntimeStack stack;
	public InstrStream instrStream;
	public List<HostFunc> hostFuncs;
    public List<CallStack> callStacks;
    public Function[] functions;
}

//ATENCION
//Los OpCodes para buscar las instrucciones por int (Mas rapido)
public class OpCodes
{
	public const int INSTR_MOV					= 0; //Está
	public const int INSTR_ADD					= 1; //Está
    public const int INSTR_SUB					= 2; //Está
    public const int INSTR_MUL					= 3; //Está
    public const int INSTR_DIV					= 4; //Está
    public const int INSTR_MOD					= 5; //Está
    public const int INSTR_EXP					= 6; //Está
    public const int INSTR_NEG					= 7; //Está
    public const int INSTR_INC					= 8; //Está
    public const int INSTR_DEC					= 9; //Está
    public const int INSTR_AND					= 10; //Está
    public const int INSTR_OR					= 11; //Está
    public const int INSTR_XOR					= 12; //Está
    public const int INSTR_NOT					= 13; //Está
    public const int INSTR_SHL					= 14; //Está
    public const int INSTR_SHR					= 15; //Está
    public const int INSTR_JMP					= 16; //Está
    public const int INSTR_JE					= 17; //Está
    public const int INSTR_JNE					= 18; //Está
    public const int INSTR_JG					= 19; //Está
    public const int INSTR_JL					= 20; //Está
    public const int INSTR_JGE					= 21; //Está
    public const int INSTR_JLE					= 22; //Está
    public const int INSTR_PUSH					= 23; //Está
    public const int INSTR_POP					= 24; //Está
    public const int INSTR_PAUSE				= 25; //Está
    public const int INSTR_EXIT					= 26; //Está
    public const int INSTR_JFR					= 27; //Está
    public const int INSTR_RET					= 28; //Está
    public const int INSTR_CALLHOST				= 29; //Está
    public const int INSTR_LN					= 30; //Está
    public const int INSTR_LOG		 			= 31; //Está
    public const int INSTR_PRINT                = 32; //Está
    public const int COUNT		 				= 33;
}
