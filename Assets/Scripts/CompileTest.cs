﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class CompileTest : MonoBehaviour 
{
	public Button compileButton;
	public InputField inputField;

	Compiler compiler = new Compiler();	
	Script script;

	// Use this for initialization
	void Awake () 
	{
		compileButton.onClick.AddListener(OnClick);	
	}

	void OnClick()
	{
		if (compiler.Compile(inputField.text))
		{
			MemoryStream ms = ByteCode.SaveToMemory(compiler.GetTables());
			
			ScriptContext context;

			if (ByteCode.Load(ms, out context))
			{
				script = new Script(context);
				script.RegisterFunc(Log);
				script.Start();
			}
		}
	}

	public void Log(Value[] values)
	{
		for	(int i=0; i< values.Length; i++){
			switch (values[i].Type)
			{
				case OpType.Int: Debug.Log(values[i].IntLiteral); break;
				case OpType.Float:Debug.Log(values[i].FloatLiteral); break;
				case OpType.String:Debug.Log(values[i].StringLiteral); break;
			}

		}
	}

	void Update()
	{
		if (script != null)
			script.RunStep();
	}
}
