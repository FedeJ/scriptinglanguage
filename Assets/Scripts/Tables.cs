﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Tables
{
    List<LabelDecl> labelsTable = new List<LabelDecl>();
    Dictionary<string, InstrDecl> instrLookUp = new Dictionary<string, InstrDecl>();
    List<VarDecl> varsTable = new List<VarDecl>();
    List<FuncDecl> funcTable = new List<FuncDecl>();
    List<Instruction> instrStream = new List<Instruction>();
    int startPC;


    public void Clear()
    {
        instrStream.Clear();
        labelsTable.Clear();
        varsTable.Clear();
        funcTable.Clear();

        startPC = -1;

    }

    public void SetStartPC(int pc)
    {
        startPC = pc;
    }

    public int GetStartPC()
    {
        return startPC;
    }

    public bool AddLabel(string ident, int idx, int funcScope)
    {
        LabelDecl label;

        if (GetLabelByName(ident, funcScope, out label)) // Already exists!
            return false;

        label = new LabelDecl();

        label.Ident = ident;
        label.Idx = idx;
        label.ScopeIdx = funcScope;

        labelsTable.Add(label);

        return true;
    }

    public bool GetLabelByName(string name, int scope, out LabelDecl label)
    {
        for (int i = 0; i < labelsTable.Count; i++)
        {
            if (labelsTable[i].Ident == name)
            {
                if (labelsTable[i].ScopeIdx == scope)
                {
                    label = labelsTable[i];
                    return true;
                }
                else
                {
                    Debug.LogWarning("El Label no esta en mismo Scope o no existe");
                }
            }
        }

        label = new LabelDecl();
        return false;
    }

    public bool AddFunc(string ident, int idx, out int scope)
    {
        FuncDecl func;
        scope = -1;

        if (GetFuncByName(ident, out func)) // Already exists!
            return false;

        func = new FuncDecl();

        func.Ident = ident;
        func.Idx = idx;
        func.ScopeIdx = scope = funcTable.Count;
        func.localVars = new List<VarDecl>();
        func.arguments = new List<VarDecl>();
        func.stackSize = func.localVars.Count;
        funcTable.Add(func);

        return true;
    }

    public bool GetFuncByName(string name, out FuncDecl func)
    {
        for (int i = 0; i < funcTable.Count; i++)
        {
            if (funcTable[i].Ident == name)
            {
                func = funcTable[i];
                return true;
            }
        }

        func = new FuncDecl();

        return false;
    }

    public bool AddArg(string ident, int funcScope) {
        VarDecl arg;

        if (GetVarByIdent(ident, out arg, funcScope))
            return false;

        arg = new VarDecl();

        arg.Ident = ident;
        arg.Idx = funcTable[funcScope].arguments.Count;
        arg.ScopeIdx = funcScope;
        arg.isArg = true;

        funcTable[funcScope].arguments.Add(arg);

        var func = funcTable[funcScope];
        func.argSize++;
        funcTable[funcScope] = func;

        return true;
    }

    public bool AddVar(string ident, int funcScope)
    {
        VarDecl var;

        if (GetVarByIdent(ident, out var, funcScope))
            return false;

        var = new VarDecl();

        var.Ident = ident;
        var.ScopeIdx = funcScope;
        var.isArg = false;


        if (funcScope == -1)
        {
            var.Idx = varsTable.Count;
            varsTable.Add(var);
        }
        else
        {
            var.Idx = funcTable[funcScope].localVars.Count;
            funcTable[funcScope].localVars.Add(var);

            var func = funcTable[funcScope];
            func.stackSize++;
            funcTable[funcScope] = func;
        }

        return true;
    }

    public bool GetVarByIdent(string ident, out VarDecl varDecl, int scope)
    {
        for (int i = 0; i < varsTable.Count; i++)
        {
            if (varsTable[i].Ident == ident)
            {
                varDecl = varsTable[i];
                return true;
            }
        }

        if (scope != -1)
        {
            for (int i = 0; i < funcTable[scope].localVars.Count; i++)
            {
                if (funcTable[scope].localVars[i].Ident == ident)
                {
                    varDecl = funcTable[scope].localVars[i];
                    return true;
                }
            }

            for (int i = 0; i < funcTable[scope].arguments.Count; i++)
            {
                if (funcTable[scope].arguments[i].Ident == ident)
                {
                    varDecl = funcTable[scope].arguments[i];
                    return true;
                }
            }
        }

        varDecl = new VarDecl();

        return false;
    }

    public List<FuncDecl> GetFuncsTable()
    {
        return funcTable;
    }

    public List<VarDecl> GetVarsTable()
    {
        return varsTable;
    }

    public bool AddInstrToStream(Instruction instruction)
    {
        instrStream.Add(instruction);

        return true;
    }

    public List<Instruction> GetInstrStream()
    {
        return instrStream;
    }

    public bool AddInstrLookUp(string instruction, int opcode, int argsCount)
    {
        instruction = instruction.ToUpper();

        if (instrLookUp.ContainsKey(instruction))
            return false;

        InstrDecl instrDecl = new InstrDecl();
        instrDecl.OpCode = opcode;
        instrDecl.ParamsCount = argsCount;

        if (argsCount > 0)
            instrDecl.ParamsFlags = new int[argsCount];

        instrLookUp[instruction] = instrDecl;

        return true;
    }

    public bool GetInstrLookUp(string instruction, out InstrDecl instrDecl)
    {
        instruction = instruction.ToUpper();

        instrDecl = new InstrDecl();

        if (!instrLookUp.ContainsKey(instruction))
            return false;

        instrDecl = instrLookUp[instruction];

        return true;
    }

    public bool SetOpType(string instruction, int argNum, int flags)
    {
        if (!instrLookUp.ContainsKey(instruction))
            return false;

        InstrDecl instrDecl = instrLookUp[instruction];

        if (argNum >= 0 && argNum < instrDecl.ParamsCount)
        {
            instrDecl.ParamsFlags[argNum] = flags;
        }
        else
        {
            return false;
        }

        instrLookUp[instruction] = instrDecl;

        return true;
    }
}
