﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


public class Script 
{
	enum State
	{
		None,
		Running,
		Pause,
	}
	private delegate void InstrDlg(Value[] values);

	ScriptContext context;

	InstrDlg[] instrExec = new InstrDlg[OpCodes.COUNT];
	State state = State.None;

	public Script()
	{
		SetInstrDlg();
	}

	public Script(ScriptContext context) 
	{
		this.context = context;
		SetInstrDlg();
	}

	private void SetInstrDlg()
	{
		// ATENCION
		// Aca se asignan las funciones al indice del array de instrucciones
		instrExec[OpCodes.INSTR_MOV] = InstrMov;
		instrExec[OpCodes.INSTR_ADD] = InstrAdd;
        instrExec[OpCodes.INSTR_SUB] = InstrSub;
        instrExec[OpCodes.INSTR_MUL] = InstrMul;
        instrExec[OpCodes.INSTR_DIV] = InstrDiv;
        instrExec[OpCodes.INSTR_MOD] = InstrMod;
        instrExec[OpCodes.INSTR_EXP] = InstrExp;
        instrExec[OpCodes.INSTR_NEG] = InstrNeg;
        instrExec[OpCodes.INSTR_INC] = InstrInc;
        instrExec[OpCodes.INSTR_DEC] = InstrDec;
        instrExec[OpCodes.INSTR_AND] = InstrAnd;
        instrExec[OpCodes.INSTR_OR] = InstrOr;
        instrExec[OpCodes.INSTR_XOR] = InstrXor;
        instrExec[OpCodes.INSTR_NOT] = InstrNot;
        instrExec[OpCodes.INSTR_SHL] = InstrShl;
        instrExec[OpCodes.INSTR_SHR] = InstrShr;
        instrExec[OpCodes.INSTR_JMP] = InstrJmp;
        instrExec[OpCodes.INSTR_JE] = InstrJe;
        instrExec[OpCodes.INSTR_JNE] = InstrJne;
        instrExec[OpCodes.INSTR_JG] = InstrJg;
        instrExec[OpCodes.INSTR_JL] = InstrJl;
        instrExec[OpCodes.INSTR_JGE] = InstrJge;
        instrExec[OpCodes.INSTR_JLE] = InstrJle;
        instrExec[OpCodes.INSTR_PUSH] = InstrPush;
        instrExec[OpCodes.INSTR_POP] = InstrPop;
        instrExec[OpCodes.INSTR_PAUSE] = InstrPause;
        instrExec[OpCodes.INSTR_EXIT] = InstrExit;
        instrExec[OpCodes.INSTR_JFR] = InstrJfr;
        instrExec[OpCodes.INSTR_RET] = InstrRet;
        instrExec[OpCodes.INSTR_CALLHOST] = InstrCallHost;
        instrExec[OpCodes.INSTR_LN] = InstrLn;
        instrExec[OpCodes.INSTR_LOG] = InstrLog;
        instrExec[OpCodes.INSTR_PRINT] = InstrPrint;
    }

	public void RunStep()
	{
		if (state == State.Running)
		{
			if (context.instrStream.Instructions == null)
				return;

			if (context.instrStream.PC >= 0 && context.instrStream.PC < context.instrStream.Instructions.Length)
			{
				Instruction op = context.instrStream.Instructions[context.instrStream.PC];

				if (instrExec[op.OpCode] != null)
					instrExec[op.OpCode](op.Values);

				context.instrStream.PC++;
			}
			else
			{
				Stop();
			}
		}
	}

	void ResetContext()
	{
		context.instrStream.PC = context.instrStream.StartPC;
		context.stack.TopStackIdx = 0;
	}

	public void Start()
	{
		if (state == State.Running)
			ResetContext();
			
		state = State.Running;
	}

	public void Stop()
	{
		state = State.None;
		ResetContext();
	}

	public void Pause(bool pause)
	{
		if (pause)
		{
			state = State.Pause;
		}
		else
		{
			state = State.Running;
		}
	}


//ATENCION
//Instrucciones que reciven los argumentos y ejecutan las tareas
#region Instructions
	private void InstrMov(Value[] values)
	{
		Value val = ResolveOpValue(values[1]);
		ResolveOpValueAndSet(0, val);
	}

	private void InstrAdd(Value[] values)
	{
		Value val1 = ResolveOpValue(values[0]);
		Value val2 = ResolveOpValue(values[1]);

		switch(val1.Type)
		{
			case OpType.Int:
				if (val2.Type == OpType.Int)
					val1.IntLiteral += val2.IntLiteral;
				else if (val2.Type == OpType.Float)
				{
					val1.Type = OpType.Float;
					val1.FloatLiteral = (float)val1.IntLiteral + val2.FloatLiteral;
				}
				else if (val2.Type == OpType.String)
				{
					val1.Type = OpType.String;
					val1.StringLiteral = val1.IntLiteral + val2.StringLiteral;
				}
			break;
			case OpType.Float:
				if (val2.Type == OpType.Int)
					val1.FloatLiteral += (float)val2.IntLiteral;
				else if (val2.Type == OpType.Float)
					val1.FloatLiteral += (float)val2.FloatLiteral;
				else if (val2.Type == OpType.String)
				{
					val1.Type = OpType.String;
					val1.StringLiteral = val1.FloatLiteral + val2.StringLiteral;
				}
			break;
			case OpType.String:
				val1.StringLiteral = val1.StringLiteral + val2.StringLiteral;
			break;
		}
		
		ResolveOpValueAndSet(0, val1);
	}

    private void InstrSub(Value[] values)
    {
        Value val1 = ResolveOpValue(values[0]);
        Value val2 = ResolveOpValue(values[1]);

        switch (val1.Type)
        {
            case OpType.Int:
                if (val2.Type == OpType.Int)
                    val1.IntLiteral -= val2.IntLiteral;
                else if (val2.Type == OpType.Float)
                {
                    val1.Type = OpType.Float;
                    val1.FloatLiteral = (float)val1.IntLiteral - val2.FloatLiteral;
                }
                break;
            case OpType.Float:
                if (val2.Type == OpType.Int)
                    val1.FloatLiteral -= (float)val2.IntLiteral;
                else if (val2.Type == OpType.Float)
                    val1.FloatLiteral -= (float)val2.FloatLiteral;
                break;
        }

        ResolveOpValueAndSet(0, val1);
    }

    private void InstrMul(Value[] values)
    {
        Value val1 = ResolveOpValue(values[0]);
        Value val2 = ResolveOpValue(values[1]);

        switch (val1.Type)
        {
            case OpType.Int:
                if (val2.Type == OpType.Int)
                    val1.IntLiteral *= val2.IntLiteral;
                else if (val2.Type == OpType.Float)
                {
                    val1.Type = OpType.Float;
                    val1.FloatLiteral = (float)val1.IntLiteral * val2.FloatLiteral;
                }
                break;
            case OpType.Float:
                if (val2.Type == OpType.Int)
                    val1.FloatLiteral *= (float)val2.IntLiteral;
                else if (val2.Type == OpType.Float)
                    val1.FloatLiteral *= (float)val2.FloatLiteral;
                break;
        }

        ResolveOpValueAndSet(0, val1);
    }

    private void InstrDiv(Value[] values)
    {
        Value val1 = ResolveOpValue(values[0]);
        Value val2 = ResolveOpValue(values[1]);

        switch (val1.Type)
        {
            case OpType.Int:
                if (val2.Type == OpType.Int)
                {
                    val1.Type = OpType.Float;
                    val1.FloatLiteral =  (float)val1.IntLiteral / (float)val2.IntLiteral;
                }
                else if (val2.Type == OpType.Float)
                {
                    val1.Type = OpType.Float;
                    val1.FloatLiteral = (float)val1.IntLiteral / val2.FloatLiteral;
                }
                break;
            case OpType.Float:
                if (val2.Type == OpType.Int)
                    val1.FloatLiteral /= (float)val2.IntLiteral;
                else if (val2.Type == OpType.Float)
                    val1.FloatLiteral /= (float)val2.FloatLiteral;
                break;
        }

        ResolveOpValueAndSet(0, val1);
    }

    private void InstrMod(Value[] values)
    {
        Value val1 = ResolveOpValue(values[0]);
        Value val2 = ResolveOpValue(values[1]);
        switch (val1.Type)
        {
            case OpType.Int:
                if (val2.Type == OpType.Int)
                    val1.IntLiteral = val1.IntLiteral % val2.IntLiteral;
                else if (val2.Type == OpType.Float)
                {
                    val1.Type = OpType.Float;
                    val1.FloatLiteral = (float)val1.IntLiteral % val2.FloatLiteral;
                }
                break;
            case OpType.Float:
                if (val2.Type == OpType.Int)
                    val1.FloatLiteral = val1.FloatLiteral % (float)val2.IntLiteral;
                else if (val2.Type == OpType.Float)
                    val1.FloatLiteral = val1.FloatLiteral % val2.FloatLiteral;
                break;
        }
        ResolveOpValueAndSet(0, val1);
    }

    private void InstrExp(Value[] values)
    {
        Value val1 = ResolveOpValue(values[0]);
        Value val2 = ResolveOpValue(values[1]);
        switch (val1.Type)
        {
            case OpType.Int:
                if (val2.Type == OpType.Int)
                    val1.IntLiteral = (int)Mathf.Pow(val1.IntLiteral, val2.IntLiteral);
                else if (val2.Type == OpType.Float)
                {
                    val1.Type = OpType.Float;
                    val1.FloatLiteral = (float)Mathf.Pow(val1.IntLiteral, val2.FloatLiteral);
                }
                break;
            case OpType.Float:
                if (val2.Type == OpType.Int)
                    val1.FloatLiteral = (float)Mathf.Pow(val1.FloatLiteral, val2.IntLiteral);
                else if (val2.Type == OpType.Float)
                    val1.FloatLiteral = (float)Mathf.Pow(val1.FloatLiteral, val2.FloatLiteral);
                break;
        }
        ResolveOpValueAndSet(0, val1);
    }

    private void InstrNeg(Value[] values) {
        Value val = ResolveOpValue(values[0]);
        if (val.Type == OpType.Int)
        {
            val.IntLiteral = ~val.IntLiteral;
        }
        ResolveOpValueAndSet(0, val);
    }

    private void InstrInc(Value[] values)
    {
        Value val = ResolveOpValue(values[0]);
        switch (val.Type) {
            case OpType.Int:
                val.IntLiteral += 1;
                break;
            case OpType.Float:
                val.FloatLiteral += 1;
                break;
        }
        ResolveOpValueAndSet(0, val);
    }

    private void InstrDec(Value[] values)
    {
        Value val = ResolveOpValue(values[0]);
        switch (val.Type)
        {
            case OpType.Int:
                val.IntLiteral -= 1;
                break;
            case OpType.Float:
                val.FloatLiteral -= 1;
                break;
        }
        ResolveOpValueAndSet(0, val);
    }

    private void InstrAnd(Value[] values)
    {
        Value val1 = ResolveOpValue(values[0]);
        Value val2 = ResolveOpValue(values[1]);
        if (val1.Type == OpType.Int && val2.Type == OpType.Int)
        {
            val1.IntLiteral = val1.IntLiteral & val2.IntLiteral;
        }
        ResolveOpValueAndSet(0, val1);
    }

    private void InstrOr(Value[] values)
    {
        Value val1 = ResolveOpValue(values[0]);
        Value val2 = ResolveOpValue(values[1]);
        if (val1.Type == OpType.Int && val2.Type == OpType.Int)
        {
            val1.IntLiteral = val1.IntLiteral | val2.IntLiteral;
        }
        ResolveOpValueAndSet(0, val1);
    }

    private void InstrXor(Value[] values)
    {
        Value val1 = ResolveOpValue(values[0]);
        Value val2 = ResolveOpValue(values[1]);
        if (val1.Type == OpType.Int && val2.Type == OpType.Int)
        {
            val1.IntLiteral = val1.IntLiteral ^ val2.IntLiteral;
        }
        ResolveOpValueAndSet(0, val1);
    }

    private void InstrNot(Value[] values)
    {
        Value val = ResolveOpValue(values[0]);
        if (val.Type == OpType.Int)
        {
            if (val.IntLiteral <= 0)
            {
                val.IntLiteral = 0;
            }
            else
            {
                val.IntLiteral = 1;
            }
        }
        ResolveOpValueAndSet(0, val);
    }

    private void InstrShl(Value[] values)
    {
        Value val = ResolveOpValue(values[0]);
        if (val.Type == OpType.Int)
        {
            val.Type = OpType.Float;
            val.FloatLiteral = val.FloatLiteral / 10;
        }
        if (val.Type == OpType.Float)
        {
            val.FloatLiteral = val.FloatLiteral / 10;
        }
        ResolveOpValueAndSet(0, val);
    }

    private void InstrShr(Value[] values)
    {
        Value val = ResolveOpValue(values[0]);
        if (val.Type == OpType.Int)
        {
            val.Type = OpType.Float;
            val.FloatLiteral = val.FloatLiteral * 10;
        }
        if (val.Type == OpType.Float)
        {
            val.FloatLiteral = val.FloatLiteral * 10;
        }
        ResolveOpValueAndSet(0, val);
    }

    private void InstrJmp(Value[] values)
    {
        Value val = ResolveOpValue(values[0]);
        context.instrStream.PC = val.IntLiteral-1;
    }

    private void InstrJe(Value[] values)
    {
        Value val1 = ResolveOpValue(values[0]);
        Value val2 = ResolveOpValue(values[1]);
        Value val3 = ResolveOpValue(values[2]);

        bool shouldJump = false;

        switch (val2.Type)
        {
            case OpType.Int:
                if (val3.Type == OpType.Int)
                    shouldJump = val2.IntLiteral == val3.IntLiteral;
                else if (val3.Type == OpType.Float)
                    shouldJump = val2.IntLiteral == (int)val3.FloatLiteral;
                else if (val3.Type == OpType.String)
                    shouldJump = val2.IntLiteral.ToString() == val3.StringLiteral;
                break;
            case OpType.Float:
                if (val3.Type == OpType.Int)
                    shouldJump = val2.FloatLiteral == val3.IntLiteral;
                else if (val3.Type == OpType.Float)
                    shouldJump = val2.FloatLiteral == val3.FloatLiteral;
                else if (val3.Type == OpType.String)
                    shouldJump = val2.FloatLiteral.ToString() == val3.StringLiteral;
                break;
            case OpType.String:
                if (val3.Type == OpType.Int)
                    shouldJump = val2.StringLiteral == val3.IntLiteral.ToString();
                else if (val3.Type == OpType.Float)
                    shouldJump = val2.StringLiteral == val3.FloatLiteral.ToString();
                else if (val3.Type == OpType.String)
                    shouldJump = val2.StringLiteral == val3.StringLiteral;
                break;
        }

        if (shouldJump)
            context.instrStream.PC = val1.IntLiteral-1;
    }

    private void InstrJne(Value[] values)
    {
        Value val1 = ResolveOpValue(values[0]);
        Value val2 = ResolveOpValue(values[1]);
        Value val3 = ResolveOpValue(values[2]);

        bool shouldJump = false;

        switch (val2.Type)
        {
            case OpType.Int:
                if (val3.Type == OpType.Int)
                    shouldJump = val2.IntLiteral != val3.IntLiteral;
                else if (val3.Type == OpType.Float)
                    shouldJump = val2.IntLiteral != (int)val3.FloatLiteral;
                else if (val3.Type == OpType.String)
                    shouldJump = val2.IntLiteral.ToString() != val3.StringLiteral;
                break;
            case OpType.Float:
                if (val3.Type == OpType.Int)
                    shouldJump = val2.FloatLiteral != val3.IntLiteral;
                else if (val3.Type == OpType.Float)
                    shouldJump = val2.FloatLiteral != val3.FloatLiteral;
                else if (val3.Type == OpType.String)
                    shouldJump = val2.FloatLiteral.ToString() != val3.StringLiteral;
                break;
            case OpType.String:
                if (val3.Type == OpType.Int)
                    shouldJump = val2.StringLiteral != val3.IntLiteral.ToString();
                else if (val3.Type == OpType.Float)
                    shouldJump = val2.StringLiteral != val3.FloatLiteral.ToString();
                else if (val3.Type == OpType.String)
                    shouldJump = val2.StringLiteral != val3.StringLiteral;
                break;
        }

        if (shouldJump)
            context.instrStream.PC = val1.IntLiteral-1;
    }

    private void InstrJg(Value[] values)
    {
        Value val1 = ResolveOpValue(values[0]);
        Value val2 = ResolveOpValue(values[1]);
        Value val3 = ResolveOpValue(values[2]);

        bool shouldJump = false;

        switch (val2.Type)
        {
            case OpType.Int:
                if (val3.Type == OpType.Int)
                    shouldJump = val2.IntLiteral > val3.IntLiteral;
                else if (val3.Type == OpType.Float)
                    shouldJump = val2.IntLiteral > (int)val3.FloatLiteral;
                break;
            case OpType.Float:
                if (val3.Type == OpType.Int)
                    shouldJump = val2.FloatLiteral > val3.IntLiteral;
                else if (val3.Type == OpType.Float)
                    shouldJump = val2.FloatLiteral > val3.FloatLiteral;
                break;
        }

        if (shouldJump)
            context.instrStream.PC = val1.IntLiteral-1;
    }

    private void InstrJl(Value[] values){
		Value val1 = ResolveOpValue(values[0]);
		Value val2 = ResolveOpValue(values[1]);
		Value val3 = ResolveOpValue(values[2]);
		
		bool shouldJump = false;

		switch(val2.Type)
		{
			case OpType.Int:
				if (val3.Type == OpType.Int)
					shouldJump = val2.IntLiteral < val3.IntLiteral;
				else if (val3.Type == OpType.Float)
					shouldJump = val2.IntLiteral < (int)val3.FloatLiteral;
			break;
			case OpType.Float:
				if (val3.Type == OpType.Int)
					shouldJump = val2.FloatLiteral < val3.IntLiteral;
				else if(val3.Type == OpType.Float)
					shouldJump = val2.FloatLiteral < val3.FloatLiteral;
			break;
		}
		if (shouldJump)
			context.instrStream.PC = val1.IntLiteral-1;
	}

    private void InstrJge(Value[] values)
    {
        Value val1 = ResolveOpValue(values[0]);
        Value val2 = ResolveOpValue(values[1]);
        Value val3 = ResolveOpValue(values[2]);

        bool shouldJump = false;

        switch (val2.Type)
        {
            case OpType.Int:
                if (val3.Type == OpType.Int)
                    shouldJump = val2.IntLiteral >= val3.IntLiteral;
                else if (val3.Type == OpType.Float)
                    shouldJump = val2.IntLiteral >= (int)val3.FloatLiteral;
                break;
            case OpType.Float:
                if (val3.Type == OpType.Int)
                    shouldJump = val2.FloatLiteral >= val3.IntLiteral;
                else if (val3.Type == OpType.Float)
                    shouldJump = val2.FloatLiteral >= val3.FloatLiteral;
                break;
        }

        if (shouldJump)
            context.instrStream.PC = val1.IntLiteral-1;
    }

    private void InstrJle(Value[] values)
    {
        Value val1 = ResolveOpValue(values[0]);
        Value val2 = ResolveOpValue(values[1]);
        Value val3 = ResolveOpValue(values[2]);

        bool shouldJump = false;

        switch (val2.Type)
        {
            case OpType.Int:
                if (val3.Type == OpType.Int)
                    shouldJump = val2.IntLiteral <= val3.IntLiteral;
                else if (val3.Type == OpType.Float)
                    shouldJump = val2.IntLiteral <= (int)val3.FloatLiteral;
                break;
            case OpType.Float:
                if (val3.Type == OpType.Int)
                    shouldJump = val2.FloatLiteral <= val3.IntLiteral;
                else if (val3.Type == OpType.Float)
                    shouldJump = val2.FloatLiteral <= val3.FloatLiteral;
                break;
        }

        if (shouldJump)
            context.instrStream.PC = val1.IntLiteral-1;
    }

    private void InstrPush(Value[] values)
	{
		Value val = ResolveOpValue(values[0]);
		Push(val);
	}

	private void InstrPop(Value[] values)
	{
		Value val = Pop();
		ResolveOpValueAndSet(0, val);
	}

    private void InstrPause(Value[] values)
    {
        Value val = ResolveOpValue(values[0]);

        Pause(val.IntLiteral > 0);
    }

    private void InstrExit(Value[] values)
	{
		Stop();
	}

    private void InstrJfr(Value[] values)  
    {
        CallStack cs = new CallStack();
        Value val = ResolveOpValue(values[0]);

        if (context.callStacks == null)
        {
            context.callStacks = new List<CallStack>();
        }

        cs.retIndex = context.instrStream.PC;
        cs.startTopIdx = context.stack.TopStackIdx;

        context.callStacks.Add(cs);

        Function func = context.functions[val.IntLiteral];

        context.stack.TopStackIdx += func.stackFrameSize;

        context.instrStream.PC = func.startIndex - 1;
    }

    private void InstrRet(Value[] values) 
    {
        CallStack callStack = context.callStacks[context.callStacks.Count - 1];
        context.callStacks.RemoveAt(context.callStacks.Count - 1);

        context.instrStream.PC = callStack.retIndex;
        context.stack.TopStackIdx = callStack.startTopIdx;
    } 

    private void InstrCallHost(Value[] values)
	{
		//Funcion
		Value val1 = ResolveOpValue(values[0]);
		//Cantidad de Argumentos
		Value val2 = ResolveOpValue(values[1]);

		Value[] valuesForFunc = values;

		for (int i = 0; i < context.hostFuncs.Count; i++)
		{
			if(context.hostFuncs[i].Ident==val1.StringLiteral){
				if (val2.IntLiteral > 0){
					valuesForFunc = new Value[val2.IntLiteral];
					for (int j = 0; j < val2.IntLiteral; j++)
					{
						valuesForFunc[j] = Pop();
					}
					context.hostFuncs[i].Func(valuesForFunc);
				}
				else
					context.hostFuncs[i].Func(valuesForFunc);
			}
			else
				Debug.Log("No hay ninguna funcion llamada" + val1.StringLiteral);
		}


	
	}

	private void InstrLog(Value[] values)
    {
        Value val1 = ResolveOpValue(values[0]);
        Value val2 = ResolveOpValue(values[1]);
        switch (val1.Type)
        {
            case OpType.Int:
                val1.Type = OpType.Float;
                val1.FloatLiteral = (float)Mathf.Log(val1.IntLiteral, val2.FloatLiteral);
                break;
            case OpType.Float:
                val1.FloatLiteral = Mathf.Log(val1.FloatLiteral, val2.FloatLiteral);
                break;
        }
        ResolveOpValueAndSet(0, val1);
    }

    private void InstrLn(Value[] values)
    {
        Value val = ResolveOpValue(values[0]);
        switch (val.Type)
        {
            case OpType.Int:
                val.Type = OpType.Float;
                val.FloatLiteral = (float)Mathf.Log(val.IntLiteral);
                break;
            case OpType.Float:
                val.FloatLiteral = Mathf.Log(val.FloatLiteral);
                break;
        }
        ResolveOpValueAndSet(0, val);
    }

    private void InstrPrint(Value[] values)
    {
        Value val = ResolveOpValue(values[0]);
        switch (val.Type)
        {
            case OpType.Int:
                Debug.Log(val.IntLiteral);
                break;
            case OpType.String:
                Debug.Log(val.StringLiteral);
                break;
            case OpType.Float:
                Debug.Log(val.FloatLiteral);
                break;
            default:
                Debug.Log("Tipo desconocido");
                break;
        }
    }


    #endregion

    public void RegisterFunc(HostFuncDlg func){
		HostFunc hostFunc = new HostFunc();
		hostFunc.Ident = func.Method.Name.ToUpper();
		hostFunc.Func = func;

		if(context.hostFuncs == null){
			context.hostFuncs = new List<HostFunc>();
		}
		context.hostFuncs.Add(hostFunc);
	}

	private void Push(Value val)
	{
		int start = context.stack.StackStartIdx;
		int top = context.stack.TopStackIdx;
		int idx = start + top;

		if (idx < context.stack.Elements.Length)
		{
			context.stack.Elements[idx] = val;
			context.stack.TopStackIdx++;
		}
		else
		{
			// TODO: Log stack overflow error!
		}
	}

	private Value Pop()
	{
		int start = context.stack.StackStartIdx;
		int top = start + context.stack.TopStackIdx;
		int idx = top - 1;
		
		Value val; 

		if (idx >= start)
		{
			val = context.stack.Elements[idx];
			context.stack.TopStackIdx--;
		}
		else
		{
			val = new Value();

			// TODO: Log stack overflow error!
		}

		return val;
	}

	private Value ResolveOpValue(Value val)
	{
		switch(val.Type)
		{
			case OpType.GlobalMemIdx:
				return context.stack.Elements[val.IntLiteral];
            case OpType.RelativeMemInx:
                return context.stack.Elements[val.IntLiteral + context.callStacks[context.callStacks.Count - 1].startTopIdx + context.stack.StackStartIdx]; // Como hacerlo relativo
            case OpType.ArgumentMemInx:
                return context.stack.Elements[context.callStacks[context.callStacks.Count - 1].startTopIdx + context.stack.StackStartIdx - val.IntLiteral - 1];
            default:
				return val;
		}
	}

	private void ResolveOpValueAndSet(int idx, Value val)
	{
        Value dst = GetOpValue(idx);
        switch (dst.Type)
		{
			case OpType.GlobalMemIdx:
                context.stack.Elements[dst.IntLiteral] = val;
                break;
            case OpType.RelativeMemInx:
                context.stack.Elements[dst.IntLiteral + context.callStacks[context.callStacks.Count - 1].startTopIdx + context.stack.StackStartIdx] = val; // Como hacerlo relativo
                break;
            case OpType.ArgumentMemInx:
                context.stack.Elements[dst.IntLiteral + context.callStacks[context.callStacks.Count - 1].startTopIdx + context.stack.StackStartIdx - val.IntLiteral - 1] = val;
                break;
        }
	}

	private Value GetOpValue(int idx)
	{
		return context.instrStream.Instructions[context.instrStream.PC].Values[idx];
	}
}
