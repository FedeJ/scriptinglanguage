﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	; COMMENT

	FUNC PEPE 

		VAR W

		POP C
		POP B
		POP A


		RET
	ENDFUNC

	LABEL:
		VAR X

		MOV X, 5

		PUSH A
		PUSH B
		PUSH C
		JSR PEPE

		JMP LABEL

 */

public class Parser
{
    Tokenizer tokenizer = new Tokenizer();

    Tables tables;

    public Parser(Tables tables)
    {
        this.tables = tables;
    }

    public void Reset()
    {
        tables.Clear();
    }

    public bool Parse(string str)
    {
        Reset();

        tokenizer.Start(str);

        // Parse Vars and Labels
        if (!Pass1())
        {
            Debug.Log("Error in Pass1");
            return false;
        }

        // Rewind tokenizer to start the
        // second pass from the beginning
        tokenizer.Rewind();

        // Parse instructions
        if (!Pass2())
        {
            Debug.Log("Error in Pass2");
            return false;
        }

        return true;
    }

    // Parse Vars, Labels and Functions
    bool Pass1()
    {
        Tokenizer.Token currentToken = tokenizer.GetNextToken();

        if (currentToken.Type == Tokenizer.TokenType.Empty)
        {
            return false;
        }

        int instrIdx = 0;
        int scope = -1;

        while (currentToken.Type != Tokenizer.TokenType.EOF && currentToken.Type != Tokenizer.TokenType.Unknown)
        {
            // ===================================================================
            // Skip end of lines
            if (currentToken.Type == Tokenizer.TokenType.EOL)
            {
                currentToken = tokenizer.GetNextToken();
            }
            // ===================================================================
            // Parse functions
            else if (currentToken.Type == Tokenizer.TokenType.Rsvd_Func)
            {
                currentToken = tokenizer.GetNextToken();

                if (currentToken.Type == Tokenizer.TokenType.Ident)
                {
                    if (!tables.AddFunc(currentToken.Lexeme, instrIdx, out scope))
                    {
                        Debug.Log("Func already exists");
                        return false;
                    }
                }
                else
                {
                    Debug.Log("Ident expected");
                    return false;
                }

                currentToken = tokenizer.GetNextToken();
            }

            else if (currentToken.Type == Tokenizer.TokenType.Rsvd_EndFunc)
            {
                if (scope != -1)
                {
                    scope = -1;
                    currentToken = tokenizer.GetNextToken();
                }
                else
                {
                    Debug.Log("EndFunc without Func ahead");
                    return false;
                }

            }

            // ===================================================================
            // Parse variables and arguments
            else if (currentToken.Type == Tokenizer.TokenType.Rsvd_Var || currentToken.Type == Tokenizer.TokenType.Rsvd_Arg)
            {
                switch (currentToken.Type)
                {
                    case Tokenizer.TokenType.Rsvd_Var:
                        currentToken = tokenizer.GetNextToken();
                        if (currentToken.Type == Tokenizer.TokenType.Ident)
                        {
                            if (!tables.AddVar(currentToken.Lexeme, scope))
                            {
                                Debug.Log("var already exists");
                                return false;
                            }
                        }
                        else
                        {
                            Debug.Log("ident expected");
                            return false;
                        }
                        break;

                    case Tokenizer.TokenType.Rsvd_Arg:
                        currentToken = tokenizer.GetNextToken();
                        if (currentToken.Type == Tokenizer.TokenType.Ident)
                        {
                            if (scope == -1)
                            {
                                Debug.Log("Args are only for functions");
                                return false;
                            }
                            if (!tables.AddArg(currentToken.Lexeme, scope))
                            {
                                Debug.Log("arg already exists");
                                return false;
                            }
                        }
                        else
                        {
                            Debug.Log("Log error ident expected");
                            return false;
                        }
                        break;
                }

                currentToken = tokenizer.GetNextToken();

                if (currentToken.Type == Tokenizer.TokenType.Ident)
                {
                    switch (currentToken.Type)
                    {
                        case Tokenizer.TokenType.Rsvd_Var:
                            if (!tables.AddVar(currentToken.Lexeme, scope))
                            {
                                Debug.Log("Log error var already exists");
                                return false;
                            }
                            break;
                        case Tokenizer.TokenType.Rsvd_Arg:
                            if (scope == 0)
                            {
                                Debug.Log("Args are only for functions");
                                return false;
                            }
                            if (!tables.AddArg(currentToken.Lexeme, scope))
                            {
                                Debug.Log("Log error arg already exists");
                                return false;
                            }
                            break;
                    }
                }


                currentToken = tokenizer.GetNextToken();
            }
            // ===================================================================
            // Parse instructions and labels			
            else if (currentToken.Type == Tokenizer.TokenType.Ident)
            {
                string ident = currentToken.Lexeme;

                currentToken = tokenizer.GetNextToken();

                // ===================================================================
                // Is it a label?
                if (currentToken.Type == Tokenizer.TokenType.Colon)
                {
                    tables.AddLabel(ident, instrIdx, scope);

                    currentToken = tokenizer.GetNextToken();
                }
                // ===================================================================
                // It's an instruction
                else
                {
                    if (scope == -1 && tables.GetStartPC() == -1)
                        tables.SetStartPC(instrIdx);

                    instrIdx++; // Increment counter

                    // Skip to next line
                    if (currentToken.Type != Tokenizer.TokenType.EOL)
                        currentToken = tokenizer.SkipToNextLine();
                }

            }
            else
            {
                Debug.Log("unexpected token");
                return false;
            }
        }
        return true;
    }

    // Parse instructions
    bool Pass2()
    {
        Instruction currentInstruction;
        Tokenizer.Token currentToken = tokenizer.GetNextToken();

        int scope = -1;

        if (currentToken.Type == Tokenizer.TokenType.Empty)
        {
            Debug.Log("Empty string");
            return false;
        }

        while (currentToken.Type != Tokenizer.TokenType.EOF && currentToken.Type != Tokenizer.TokenType.Unknown)
        {
            // ===================================================================
            // Skip end of lines
            if (currentToken.Type == Tokenizer.TokenType.EOL)
            {
                currentToken = tokenizer.GetNextToken();
            }
            // ===================================================================
            // Skip variables declaration 
            else if (currentToken.Type == Tokenizer.TokenType.Rsvd_Var)
            {
                currentToken = tokenizer.GetNextToken(); // Skip the VAR reserved word

                currentToken = tokenizer.GetNextToken(); // Skip VAR's identifier
            }
            // Skip arguments declaration 
            else if (currentToken.Type == Tokenizer.TokenType.Rsvd_Arg)
            {
                currentToken = tokenizer.GetNextToken(); // Skip the ARG reserved word

                currentToken = tokenizer.GetNextToken(); // Skip ARG's identifier
            }
            // ===================================================================
            // Skip Func declaration 
            else if (currentToken.Type == Tokenizer.TokenType.Rsvd_Func)
            {
                currentToken = tokenizer.GetNextToken(); // Skip the FUNC reserved word

                FuncDecl func;

                if (!tables.GetFuncByName(currentToken.Lexeme, out func))
                {
                    return false;
                }

                scope = func.ScopeIdx;
                currentToken = tokenizer.GetNextToken(); // Skip FUNC's identifier

            }
            // ===================================================================
            // Skip EndFunc declaration 
            else if (currentToken.Type == Tokenizer.TokenType.Rsvd_EndFunc)
            {
                scope = -1;
                currentToken = tokenizer.GetNextToken(); // Skip the ENDFUNC reserved word
            }
            // ===================================================================
            // Parse instructions and labels			
            else if (currentToken.Type == Tokenizer.TokenType.Ident)
            {
                string ident = currentToken.Lexeme;

                currentToken = tokenizer.GetNextToken();

                // ===================================================================
                // Is it a label? Skip it
                if (currentToken.Type == Tokenizer.TokenType.Colon)
                {

                    currentToken = tokenizer.GetNextToken();
                }
                // ===================================================================
                // It's an instruction
                else
                {
                    InstrDecl instr;

                    if (!tables.GetInstrLookUp(ident, out instr))
                    {
                        // TODO: Log error: syntax error
                        Debug.Log("Syntax error");
                        return false;
                    }

                    currentInstruction = new Instruction();
                    currentInstruction.OpCode = instr.OpCode;

                    if (instr.ParamsCount > 0)
                        currentInstruction.Values = new Value[instr.ParamsCount];

                    // ===================================================================
                    // Parse params
                    for (int i = 0; i < instr.ParamsCount; i++)
                    {
                        // We have to skip the ','
                        if (i > 0)
                        {
                            currentToken = tokenizer.GetNextToken();
                            if (currentToken.Type != Tokenizer.TokenType.Comma)
                            {
                                // TODO: Log error: Comma expected.
                                Debug.Log("Comma Expected");
                                return false;
                            }

                            currentToken = tokenizer.GetNextToken();
                        }

                        Tokenizer.TokenType t = currentToken.Type;
                        int flags = instr.ParamsFlags[i];

                        // ===================================================================
                        // Is it a variable or label?
                        if (t == Tokenizer.TokenType.Ident)
                        {
                            if ((flags & OpFlags.MemIdx) != 0)
                            {
                                VarDecl varDecl;

                                if (!tables.GetVarByIdent(currentToken.Lexeme, out varDecl, scope))
                                {
                                    Debug.Log("variable doesn´t exist");
                                    return false;
                                }
                                if (varDecl.ScopeIdx != -1)
                                {
                                    if (varDecl.isArg)
                                    {
                                        currentInstruction.Values[i].Type = OpType.ArgumentMemInx;
                                    }
                                    else
                                    {
                                        currentInstruction.Values[i].Type = OpType.RelativeMemInx;
                                    }
                                }
                                else
                                {
                                    currentInstruction.Values[i].Type = OpType.GlobalMemIdx;
                                }
                                currentInstruction.Values[i].StackIndex = varDecl.Idx;
                            }
                            else if ((flags & OpFlags.InstrIdx) != 0)
                            {
                                LabelDecl label;

                                if (!tables.GetLabelByName(currentToken.Lexeme, scope, out label))
                                {
                                    // TODO: Log error: label doesn´t exist
                                    Debug.Log("label doesn´t exist");
                                    return false;
                                }

                                currentInstruction.Values[i].Type = OpType.InstrIdx;
                                currentInstruction.Values[i].InstrIndex = label.Idx;
                            }
                            else if ((flags & OpFlags.FuncIdx) != 0)
                            {
                                FuncDecl func;
                                if (!tables.GetFuncByName(currentToken.Lexeme, out func))
                                {
                                    Debug.Log("Function "+ currentToken.Lexeme + " doesn´t exist");
                                    return false;
                                }

                                currentInstruction.Values[i].Type = OpType.FuncIdx;
                                currentInstruction.Values[i].IntLiteral = func.ScopeIdx;
                            }
                            else if ((flags & OpFlags.HostAPICallStr) != 0)
                            {
                                // TODO: host api calls
                                currentInstruction.Values[i].Type = OpType.HostAPICallStr;
                                currentInstruction.Values[i].StringLiteral = currentToken.Lexeme;
                            }
                        }
                        // ===================================================================
                        // Is it a literal value?
                        else if (t == Tokenizer.TokenType.Number || t == Tokenizer.TokenType.String)
                        {
                            if ((flags & OpFlags.Literal) == 0)
                            {
                                // TODO: Log error: doesn´t allow literals
                                Debug.Log("doesn´t allow literals");
                                return false;
                            }

                            if (t == Tokenizer.TokenType.Number)
                            {
                                if (StringUtil.IsStringFloat(currentToken.Lexeme))
                                {
                                    float val = 0;

                                    currentInstruction.Values[i].Type = OpType.Float;

                                    if (float.TryParse(currentToken.Lexeme, out val))
                                        currentInstruction.Values[i].FloatLiteral = val;
                                    else
                                    {
                                        // TODO: log error: error parsing float value
                                        Debug.Log("error parsing float value");
                                        return false;
                                    }
                                }
                                else if (StringUtil.IsStringInt(currentToken.Lexeme))
                                {
                                    int val = 0;

                                    currentInstruction.Values[i].Type = OpType.Int;

                                    if (int.TryParse(currentToken.Lexeme, out val))
                                        currentInstruction.Values[i].IntLiteral = val;
                                    else
                                    {
                                        // TODO: log error: error parsing int value
                                        Debug.Log("error parsing int value");
                                        return false;
                                    }
                                }
                                else if (StringUtil.IsStringHex(currentToken.Lexeme))
                                {
                                    currentInstruction.Values[i].Type = OpType.Int;
                                    currentInstruction.Values[i].IntLiteral = StringUtil.StrHexToInt(currentToken.Lexeme);
                                }
                                else
                                {
                                    // TODO: log error: error parsing literal value
                                    Debug.Log("error parsing literal value");
                                    return false;
                                }
                            }
                            else
                            {
                                currentInstruction.Values[i].Type = OpType.String;
                                currentInstruction.Values[i].StringLiteral = currentToken.Lexeme;
                            }
                        }
                        else
                        {
                            // TODO: log error: unexpected token
                            Debug.Log("error parsing float value");
                            return false;
                        }
                    }

                    // Add the instruction to the stream
                    tables.AddInstrToStream(currentInstruction);

                    // Skip to next token
                    currentToken = tokenizer.GetNextToken();
                }
            }
            else
            {
                // TODO: log error: unexpected token
                Debug.Log("unexpected token");
                return false;
            }
        }

        return true;
    }

}
